package nl.tudelft.example.context

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloWorldRouter {

	@RequestMapping("/")
	fun thing(): String {
		return "Hello, world!"
	}
}