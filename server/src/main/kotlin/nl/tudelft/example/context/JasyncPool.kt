package nl.tudelft.example.context

import com.github.jasync.sql.db.ConnectionPoolConfiguration
import com.github.jasync.sql.db.mysql.MySQLConnectionBuilder
import org.springframework.stereotype.Component

@Component
class DB {

	val connection = MySQLConnectionBuilder.createConnectionPool(
		ConnectionPoolConfiguration(

		)
	)

}