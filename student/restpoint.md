## RESTPOINTS

### STUDENT
#### Authentication
- url: /v1/authenticate/login
- input: { username: String, password: String }
- response: {}
- responseCode: 201

##### Misschien niet nodig since SSO
- url: /v1/authenticate/register
- input: { username: String, password: String, email: Email }
- response: { username: String, password: String }
- responseCode: 200

- url: /v1/autehtnicate/logout
- input: {}
- response: {}
- responseCode: 200

#### Quiz
- url: /v1/quiz/all
- input: {}
- response: [{ Question }]
- responseCode: 200

- url: /v1/quiz/addQuestion
- input: { Question }
- response: {}
- respondeCode: 200


### TEACHER

3/5 -> week 2 (planning)
10/5 -> week 3 (sprint, rest points initialisated and tested, database setup, home screen, teacher question creation v1)
17/5 -> week 4 (sprint, database connection, JPA, quiz (multichoice), teacher question creation v2)
24/5 -> week 5 (sprint, switch, prepare data, quiz (drag drop, connect the dots), teacher listen to data prepare dashboard)
31/5 -> week 6 (sprint)
7/6 -> week 7 
14/6 -> week 8 (fat, cap)
21/6 -> week 9 (final deployment)
28/6 -> week 10 (final date)

