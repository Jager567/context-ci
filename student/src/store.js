import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers';

const logger = store => next => action => {
	let result = next(action)
	console.warn('dispatching next store', store.getState())
	return result;
}

const crashReporter = () => next => action => {
	try {
		return next(action)
	} catch (err) {
		console.error('error', err)
	}
}

const initialState = {}
const middleware = [crashReporter, logger, thunk]
const store = createStore(
	rootReducer,
	initialState,
	compose(
		applyMiddleware(...middleware),
		window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
	)
);

export default store