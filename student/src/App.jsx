import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import  MyProvider from "./Context";
import Main from './components/Main'

import { App } from 'framework7-react';

import { Provider } from 'react-redux'
import store from './store'


const MyApp = () => {
	return (
		<Provider store={store}>
			<MyProvider>
				<BrowserRouter>
					<App>
						<Main />
					</App>
				</BrowserRouter>
			</MyProvider>
		</Provider>
	);
}

export default MyApp;
