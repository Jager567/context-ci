export const FETCH_POSTS = 'FETCH_POST'
export const NEW_POST = 'NEW_POST'
export const DELETE_POST = 'DELETE_POST'
export const SEARCH_POST = 'SEARCH_POST'

// Questions
export const FETCH_QUESTIONS = 'FETCH_QUESTIONS'
export const CREATE_QUESTION = 'CREATE_QUESTION'
export const FILTER_QUESTION = 'FILTER_QUESTION'
export const NEXT_QUESTION = 'NEXT_QUESTION'

// TODO
export const CREATE_TODO = 'CREATE_TODO'
export const ALL_TODOS = 'GET_ALL_TODOS'
export const DELETE_TODO = 'REMOVE_TODO'
export const UPDATE_TODO = 'UPDATE_TODO'