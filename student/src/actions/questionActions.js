import { FETCH_QUESTIONS, NEXT_QUESTION } from './types'

export const fetchQuestions = () => dispatch => {
	dispatch({
		type: FETCH_QUESTIONS,
		payload: {
			questions: [{
				id: 2,
				question: '1 + 1 is',
				answer: [{
					id: 1,
					text: '2',
					correct: true
				}, {
					id: 2,
					text: '3',
					correct: false
				}]
			}],
			currentQuestion: {
				id: 1,
				question: 'Sum question',
				answer: [{
					id: 1,
					text: 'Fuck',
					correct: true
				}, {
					id: 2,
					text: 'That',
					correct: false
				}, {
					id: 3,
					text: 'Another one',
					correct: false
				}]
			}
		}
	})
}

export const submitQuestion = () => dispatch => {
	dispatch({
		type: NEXT_QUESTION,
		payload: {
			questions: [{
				id: 1,
				question: 'Sum question',
				answer: [{
					id: 1,
					text: 'Fuck',
					correct: true
				}, {
					id: 2,
					text: 'That',
					correct: false
				}, {
					id: 3,
					text: 'Another one',
					correct: false
				}]
			}],
			currentQuestion: {
				id: 2,
				question: '1 + 1 is',
				answer: [{
					id: 1,
					text: '2',
					correct: true
				}, {
					id: 2,
					text: '3',
					correct: false
				}]
			}
		}
	})
}