import { FETCH_POSTS, NEW_POST, DELETE_POST, SEARCH_POST } from "./types";

export const fetchPosts = () => dispatch =>  {
	fetch('https://jsonplaceholder.typicode.com/posts')
		.then(res => res.json())
		.then(posts => dispatch({
			type: FETCH_POSTS,
			payload: posts
		}))
}

export const createPost = postData => dispatch => {
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'post',
		header: {
			'content-type': 'application/json'
		},
		body: JSON.stringify(postData)
	})
		.then(res => res.json())
		.then(post => dispatch({
			type: NEW_POST,
			payload: post
		}))
}

export const deletePost = () => dispatch => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'delete'
	})
		.then(res => res.json())
		.then(post => dispatch({
			type: DELETE_POST,
			payload: post
		}))
}

export const findPost = id => dispatch => {
	fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
		.then(res => res.json())
		.then(posts => {
			dispatch({
				type: SEARCH_POST,
				payload: posts.filter(el => el.id === 1)
			})
		})
}