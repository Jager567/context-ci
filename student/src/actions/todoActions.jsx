import { ALL_TODOS, CREATE_TODO, UPDATE_TODO } from "./types";

export const fetchTodos = () => dispatch => {
	dispatch({
		type: ALL_TODOS,
		payload: [
			{ "id": 1, "title": "hello world"},
			{ "id": 2, "title": "goodnight world"},
			{ "id": 3, "title": "get drunk"}
		]
	})
}

export const createTodo = todo => dispatch => {
	dispatch({
		type: CREATE_TODO,
		payload: todo
	})
}

export const updateTodo = todo => dispatch => {
	dispatch({
		type: UPDATE_TODO,
		payload: todo
	})
}