import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createTodo } from "../actions/todoActions";

import { ListInput, List, Button } from "framework7-react";

export class TodoForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			title: ''
		}

		this.onChange = this.onChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	}

	onChange(e) {
		this.setState({[e.target.name]: e.target.value})
	}

	onSubmit(e) {
		e.preventDefault()
		this.props.createTodo({title: this.state.title});
	}

	render() {
		return (
			<List noHairlinesMd form>
				<ListInput
					outline
					floatingLabel
					label="Todo"
					type="text"
					placeholder="new todo"
					value={this.state.title}
					name='title'
					onInput={this.onChange} />
				<Button type="submit" onClick={this.onSubmit}>Submit</Button>
			</List>
		)
	}
}

TodoForm.propTypes = {
	createTodo: PropTypes.func.isRequired
}

const mapDispatchToProps = {
	createTodo
}

export default connect(null, mapDispatchToProps)(TodoForm)
