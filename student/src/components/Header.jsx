import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Header extends Component {
	render() {
		return (
			<div>
				<button onClick={this.props.context.changeLanguage} data-language="bl">This will change the language</button>
			</div>
		)
	}
}

Header.propTypes = {
	context: PropTypes.func.isRequired
}
