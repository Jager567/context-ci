import React, { Fragment } from 'react'
import { Route } from 'react-router-dom'
import { MyContext } from "../Context";

import HomePage from '../pages/HomePage';
import AboutPage from '../pages/AboutPage';
import TodoPage from '../pages/TodoPage'

const Main = () => {
	return (
		<MyContext.Consumer>
			{ context =>
				<Fragment>
					<Route exact={true} path="/" render={(props) => <HomePage {...props} context={context} />} />
					<Route path="/about" component={AboutPage}/>
					<Route path="/todos" component={TodoPage}/>
				</Fragment>
			}
		</MyContext.Consumer>
	)
}

export default Main
