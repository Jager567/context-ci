import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { ListItem, ListInput } from 'framework7-react';
import { updateTodo } from "../actions/todoActions";

export class Todo extends Component {
	constructor(props) {
		super(props)

		this.state = {
			title: props.title,
			id: props.id,
			edit: false
		}

		this.onChange = this.onChange.bind(this)
		this.onBlur = this.onBlur.bind(this)
		this.onClick = this.onClick.bind(this)
	}

	onChange(e) {
		this.setState({[e.target.name]: e.target.value})
	}

	onBlur(e) {
		e.preventDefault()
		this.setState({edit: false})
		this.props.updateTodo({id: this.state.id, title: this.state.title})
	}

	onClick(e) {
		e.preventDefault()
		this.setState({edit: true})
	}

	calculate = (x, y) => {
		return x + y
	}

	render() {
		const toRender = this.state.edit ? <ListInput
			outline
			autofocus
			floatingLabel
			label="New title"
			type="text"
			placeholder={this.state.title}
			value={this.state.title}
			name='title'
			onInput={this.onChange}
			onBlur={this.onBlur}
		/> :
			<ListItem title={this.state.title} onClick={this.onClick}/>


		return (
            <>{toRender}</>
		)
	}
}

Todo.propTypes = {
	title: PropTypes.string.isRequired,
	id: PropTypes.number.isRequired,
	updateTodo: PropTypes.func.isRequired
}

const mapDispatchToProps = {
	updateTodo
}

export default connect(null, mapDispatchToProps)(Todo)
