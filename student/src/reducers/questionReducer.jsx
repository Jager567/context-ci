import { FETCH_QUESTIONS, NEXT_QUESTION } from '../actions/types'

const initialState = {
	questions: [],
	currentQuestion: {}
}

export default (state = initialState, { type, payload }) => {
	switch(type) {
	case FETCH_QUESTIONS: return {
		...state,
		questions: payload.questions,
		currentQuestion: payload.currentQuestion
	}
	case NEXT_QUESTION: return {
		...state,
		questions: payload.questions,
		currentQuestion: payload.currentQuestion
	}
	default: return state
	}
}

