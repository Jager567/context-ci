import { combineReducers } from 'redux'
import postReducer from './postReducer'
import questionReducer from './questionReducer';
import todoReducer from './todoReducer'

export default combineReducers({
	posts: postReducer,
	questions: questionReducer,
	todo: todoReducer
})