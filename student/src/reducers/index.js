import { combineReducers } from 'redux'
import postReducer from './postReducer'
import questionReducer from './questionReducer';

export default combineReducers({
	posts: postReducer,
	questions: questionReducer
})