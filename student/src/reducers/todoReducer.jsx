import { ALL_TODOS, CREATE_TODO, UPDATE_TODO } from '../actions/types'

const initialState = {
	items: []
}

export default (state = initialState, { type, payload }) => {
	switch(type) {
	case ALL_TODOS: return {
		...state,
		items: payload
	}
	case CREATE_TODO: return {
		...state,
		items: [...state.items, {id: state.items.length + 1, title: payload.title}]
	}
	case UPDATE_TODO: {
		const el = state.items.find(el => el.id === payload.id)
		el.title = payload.title
		return {
			...state,
			items: state.items
		}
	}
	default: return state
	}
}