import React, { Component } from 'react'
import PropTypes from 'prop-types'

export const MyContext = React.createContext()

export default class MyProvider extends Component {
	constructor(props) {
		super(props)

		this.state = {
			language: 'en'
		}
	}

    changeLanguage = (lang) => this.setState({language: lang.target.dataset.language === this.state.language ? 'en' : 'bl'})
    validLanguage = (key, lang, defaultLang) => lang[key] === undefined ? defaultLang[key] : lang[key]

    render() {
    	return (
    		<MyContext.Provider value={{
    			state: this.state,
    			changeLanguage: this.changeLanguage,
    			validLanguage: this.validLanguage
    		}}>{this.props.children}
    		</MyContext.Provider>
    	)
    }
}

MyProvider.propTypes = {
	children: PropTypes.array
}
