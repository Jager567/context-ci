import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchTodos } from "../actions/todoActions";
import { Page, Fab, Icon, FabButton, FabButtons, List, BlockTitle } from 'framework7-react';
import TodoForm from '../components/TodoForm';
import Todo from '../components/Todo';

export class TodoPage extends Component {
	componentWillMount() {
		this.props.fetchTodos()
	}

	componentWillUpdate() {
		console.warn('hello world')
	}

	render() {
		const todo = this.props.todos.map(el => (
			<Todo key={el.id} title={el.title} id={el.id}/>
		))

		return (
			<Page>
				<BlockTitle>Todos</BlockTitle>
				<TodoForm/>
				<List mediaList inset><ul>{todo}</ul></List>
				<Fab position="right-top" slot="fixed" color="pink">
					<Icon ios="f7:add" aurora="f7:add" md="material:add"></Icon>
					<Icon ios="f7:close" aurora="f7:close" md="material:close"></Icon>
					<FabButtons position="left">
						<FabButton>1</FabButton>
						<FabButton>2</FabButton>
						<FabButton>3</FabButton>
					</FabButtons>
				</Fab>
			</Page>
		)
	}
}

TodoPage.propTypes = {
	fetchTodos: PropTypes.func.isRequired,
	todos: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
	todos: state.todo.items
})

const mapDispatchToProps = {
	fetchTodos
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoPage)

