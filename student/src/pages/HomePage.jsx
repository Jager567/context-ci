import React, { Component } from 'react'
import { MyContext } from "../Context";
import { Page, Navbar, BlockTitle, List, ListItem, Icon, Statusbar, Block, Panel, Segmented, Button } from 'framework7-react';
import PropTypes from 'prop-types'

const json = require('../language.json')

export default class HomePage extends Component {
	testLanguage(key, context) {
		return this.props.context.validLanguage(key, json[context.state.language], json['en'])
	}

	render() {
		return (
			<MyContext.Consumer>
				{(context) => (
					<Page>
						<Statusbar />
						<Panel right reveal themeDark>
							<Navbar title="right navigation" />
							<Block>This is some block text</Block>
						</Panel>
						<Segmented>
							<Button panelOpen="right">Open the right panel</Button>
						</Segmented>
						<Navbar tile="title" />
						<BlockTitle>Title</BlockTitle>
						<List>
							<ListItem title={this.testLanguage('title', context)} after="CEO">
								<Icon slot="media" icon="demo-list-icon"></Icon>
							</ListItem>
						</List>
					</Page>
				)}
			</MyContext.Consumer>
		)
	}
}

HomePage.propTypes = {
	context: PropTypes.func.isRequired
}
