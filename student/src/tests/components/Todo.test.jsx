import React from 'react'
import { shallow } from 'enzyme';
import { Todo } from "../../components/Todo";

describe('>>>H O M E --- Shallow Render REACT COMPONENTS',()=>{
	let wrapper

	beforeEach(() =>  {
		wrapper = shallow(<Todo title={"hello"} updateTodo={() => {}} id={1}/>)
	})

	it('testing should still work', () => {
		expect(2).toEqual(2)
	})

	it('should be able to calculate the sum', () => {
		expect(wrapper.instance().calculate(1, 2)).toEqual(3)
	})
});