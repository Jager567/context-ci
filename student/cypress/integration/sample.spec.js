/// <reference types="Cypress" />

context('Window', () => {
    beforeEach(() => {
    	cy.visit('https://www.vu.nl/nl/index.aspx')
    })
	
	it('cy should be able to open the dropdown menu', () =>  {
		cy.get('.navigation').should('not.be.visible')

		cy
			.get('.navbar-toggle')
			.should('have.class', 'navbar-toggle')
			.click()
		
		cy.get('.navigation').should('be.visible')
	})

	it('cy should be able to interact with the slider', () => {
		cy
			.get('.swiper-button .swiper-button-next')
			.should('be.visible')
			.click()
	})
})
  